var table;
(function ($, Drupal) {
    "use strict";
    Drupal.behaviors.ajax_datatables = {
      attach: function (context, settings) {
          var obj_tables = drupalSettings.ajax_datatables.data;
          $.each(obj_tables, function (i) {
            var value = obj_tables[i];
            var id = value.id;
            var filterwrapperID = value.filterwrapperID; 
            var paging = (typeof value.paging === "undefined") ? true : value.paging;
            var ordering = (typeof value.ordering === "undefined") ? true : value.ordering;
            var searching = (typeof value.searching === "undefined") ? true : value.searching;
            var pageLength = (typeof value.pageLength === "undefined") ? true : value.pageLength;
            var fixedHeader = (typeof value.fixedHeader === "undefined") ? true : value.fixedHeader;
            var columns = value.columns;          
            var url = value.url;
            var queryStr = getQueryStrings();
            table = $(id).DataTable({
                "processing": true,
                "serverSide": true,
                "serverMethod": 'post',                
                "ajax": {
                    "url": drupalSettings.path.baseUrl + url + "?" + $.param(queryStr)
                },
                "columns": columns,
                "paging" : paging,
                "ordering" : ordering,
                "searching": searching,
                "pageLength": pageLength, 
                "fixedHeader": fixedHeader,   
                "destroy" : true                       
            });  
            $(filterwrapperID).on('change', 'select', function (e) {
              var str_query = [];
              var params = '';      
              var queryStr = getQueryStrings();      
              console.log(queryStr)            
              $(filterwrapperID + " :selected").each(function() {
                params = $(this).parent().attr('name');
                str_query.push({name: params, value: this.value});
              });
              $.each( queryStr, function( key, value ) {
                str_query.push({name: key, value: value});                    
              });
              var query = $.param(str_query);
              table.ajax.url(drupalSettings.path.baseUrl + url + "?" + query);
              table.ajax.reload();
            }); 
        });
      }
    };
    function getQueryStrings() {
      var queries = {};
      $.each(document.location.search.substr(1).split('&'),function(c,q){
        var i = q.split('=');
        if (i.length != 1) {
          queries[i[0].toString()] = i[1].toString();
        }
      });
      return queries;
    }
  })(jQuery, Drupal);
