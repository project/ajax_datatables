<?php

namespace Drupal\ajax_datatables\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element to display ajax datatables.
 *
 * @RenderElement("ajax_datatables")
 */
class AjaxDatatablesTheming extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {    
    return [
      '#attached' => [
        'library' => [
          'ajax_datatables/ajax_datatables.default',
        ],
      ],
      '#data' => [],
      '#type' => [],     
      '#pre_render' => [
        [self::class, 'preRenderAjaxDatatablesTheming'],
      ],      
    ];
  }

  /**
   * Element pre render callback.
   */
  public static function preRenderAjaxDatatablesTheming($element) {
    $element['#attached']['drupalSettings']['ajax_datatables'] = [      
      'data' => $element['#data'],       
    ];
    return $element;
  }

}
