<?php

namespace Drupal\ajax_datatables\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use \Symfony\Component\HttpFoundation\JsonResponse;

/**
  * Class UserDetailsController.
 */
class UserDetailsController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager  = $entity_type_manager;    
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')      
    );
  }

  /**
   * Return User Details.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * The formatted JSON response.
   */
  public function getUserDetails() {
    $response_array = [];
    $data = [];
    $ids = \Drupal::entityQuery('user')->condition('status', 1)->execute();
    $users = User::loadMultiple($ids);
    foreach($users as $key => $user) {
      $data[$key]['uid'] = $user->get('uid')->value;
      $data[$key]['name'] = $user->get('name')->value;
      $data[$key]['mail'] = $user->get('mail')->value;
      $roles = [];
      foreach($user->get('roles')->getValue() as $value) {
        $obj_role = Role::load($value['target_id']);        
        $roles[] = $obj_role->get('label');
      }
      $data[$key]['role'] = implode(',' , $roles);
    }    
    // Initialize the response array.
    $results = [];
    $results = array_values($data);
    $columnIndex = $_POST['order'][0]['column'];
    $columnName = $_POST['columns'][$columnIndex]['data'];
    $orderType = $_POST['order'][0]['dir']; // desc or asc    
    if(count($results) > 0) {
      if($columnName == 'name') { // Sort By Name
        $results = self::sortUserByName($results, $orderType); 
      } 
      else if($columnName == 'uid') { // Sort By Uid
        $results = self::sortUserByID($results, $orderType);
      }
      else if($columnName == 'mail') { // Sort By Mail
        $results = self::sortUserByEmail($results, $orderType);
      }
    }
    
    $draw = $_POST['draw'];
    $row = $_POST['start'];
    $rowperpage = $_POST['length']; // Rows display per page
    $totalRecords = count($results);
    $totalRecordwithFilter = $totalRecords;
    if(count($data) > 0) {
      $results = array_slice($results, $row, $rowperpage);
    }
    $response = array(
      "draw" => $draw,
      "iTotalRecords" => $totalRecords,
      "iTotalDisplayRecords" => $totalRecordwithFilter,
      "aaData" => $results
    );    
   
    $response = new JsonResponse($response);    

    return $response;
  } 

  public static function sortUserByName($results, $order) {
    if($order == 'desc') {
      $keys = array_column($results, 'name');   
      array_multisort($keys, SORT_DESC, $results, SORT_FLAG_CASE);
    }
    else {
      $keys = array_column($results, 'name');   
      array_multisort($keys, SORT_ASC, $results, SORT_FLAG_CASE);
    }
    return $results;
  }

  public static function sortUserByID($results, $order) {
    if($order == 'desc') {
      $keys = array_column($results, 'uid');   
      array_multisort($keys, SORT_DESC, $results, SORT_NUMERIC);
    }
    else {
      $keys = array_column($results, 'uid');   
      array_multisort($keys, SORT_ASC, $results, SORT_NUMERIC);
    }
    return $results;
  }

  public static function sortUserByEmail($results, $order) {
    if($order == 'desc') {
      $keys = array_column($results, 'mail');   
      array_multisort($keys, SORT_DESC, $results, SORT_FLAG_CASE);
    }
    else {
      $keys = array_column($results, 'mail');   
      array_multisort($keys, SORT_ASC, $results, SORT_FLAG_CASE);
    }
    return $results;
  }

}
