<?php

namespace Drupal\ajax_datatables\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;

/**
 * Provides a 'Ajax Datatables' block.
 *
 * @Block(
 *   id = "ajax_datatables_block",
 *   admin_label = @Translation("Ajax Datatables Block"),
 *   category = @Translation("Ajax Datatables Block")
 * )
 */
class AjaxDatatablesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Request stack.
   *
   * @var RequestStack
   */
  protected $request;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              RequestStack $request
                             ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['ajax_datatables']['#data'] = [
      'ajax-user-list' => [
        'id' => '#ajax-user-list',
        'filterwrapperID' => '#datatablefilter',
        'paging' => true,
        'ordering' => true,
        'pageLength' => 10,
        'fixedHeader' => [
          'header' => true,
        ],
        'columns' => [
          [
            'data' => 'uid',
            'className' => 'user-list-uid',                            
            'searchable' => false,
            'visible' => true,
            'defaultOrder' => true,
            'sortOrder' => 'desc'
          ],
          [
            'data' => 'name'              
          ],
          [
            'data' => 'mail',
          ],
          [
            'data' => 'role',
            'orderable' => false,
          ]          
        ],
        'url' => 'api/rest/ajax-datatables'
      ],
    ];
    
    $build['ajax_datatables']['#type'] = 'ajax_datatables';
    $build['ajax_datatables']['#theme'] = 'ajax_datatables';

    return $build;
  }

}